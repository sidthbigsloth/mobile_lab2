package com.example.sindre.lab2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener {

    public String[]titles =new String[100];//{"GOOGLE","FACEBBOK","TWITTER","","","","","","","","","","","","","","","","","20","21","","","","","","",""};
    public String[]subtitle=new String[100];//{"google is large company","Facebook is a socialsite networking","Twitter is indian company","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
    public String[]links = new String[100];//{"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};

    public SwipeRefreshLayout mSwipeLayout;
    ListView list;
    List<RowData> rowData;

    public SharedPreferences prefs;

    private Handler handler;
    private Runnable runner;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mSwipeLayout = (SwipeRefreshLayout) this.findViewById(R.id.refreshList);
        mSwipeLayout.setOnRefreshListener( MainActivity.this);

        startHandler();
        /**/

        /**/

    }

    public void startHandler()
    {
        prefs = getSharedPreferences("prefs", Activity.MODE_PRIVATE);

        handler = new Handler();
        runner = new Runnable() {
            @Override
            public void run() {
                FetchRSS();
                int rates[] = getResources().getIntArray(R.array.pref_sync_frequency_values);
                handler.postDelayed(runner, rates[prefs.getInt("refresh", 0)]*6*1000);
            }
        };

        handler.post(runner);
    }

    @Override
    public void onRefresh()
    {
        mSwipeLayout.setColorScheme(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        startHandler();
        mSwipeLayout.setRefreshing(false);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                startHandler();
            }
        }
    }

    public void goToSettings(View view)
    {/**/
        Intent intent = new Intent(this, SettingsActivity.class);
        startActivityForResult(intent, 1);/**/
    }

    protected void Update()
    {
        SharedPreferences prefs = getSharedPreferences("prefs", Activity.MODE_PRIVATE);
        int maxs[] = getResources().getIntArray(R.array.pref_sync_item_amount);
        int max = maxs[prefs.getInt("max", 1)];

        if(max > titles.length)
            max = titles.length;
        rowData =new ArrayList<>();
        for(int i=0;i<max;i++){
            RowData data =new RowData();
            data.setSubtitle(subtitle[i]);
            data.setTitle(titles[i]);
            rowData.add(data);
        }
        list = findViewById(R.id.list);
        MyAdapter adapter = new MyAdapter(this, rowData);
        list.setAdapter(adapter);

        list.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {

            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {


                Uri uri = Uri.parse(links[position]);

                Intent intent = new Intent (MainActivity.this, WebViewActivity.class);
                String message = uri.toString();
                intent.putExtra("URL", message);
                startActivity (intent);
            }
        });
    }

    protected void FetchRSS()
    {
        SharedPreferences prefs = getSharedPreferences("prefs", Activity.MODE_PRIVATE);
        URL url = null;
        try {
            url = new URL(prefs.getString("url", "https://www.vg.no/rss/feed/?categories=1101%2C1107&limit=100&format=rss&keywords="));
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        new RSSLoader().execute(url);


    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    private class RSSLoader extends AsyncTask<URL, Void, Void>
    {
        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();
        }
/**/
        @Override
        protected void onPostExecute(Void result)
        {
            Update();
        }
/**/
        @Override
        protected Void doInBackground(URL... urls)
        {
            int count = urls.length;
            URL url = urls[0];

            XmlPullParserFactory factory = null;
            try {
                factory = XmlPullParserFactory.newInstance();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }
            factory.setNamespaceAware(true);
            XmlPullParser parser = null;
            try {
                parser = factory.newPullParser();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }

            try {
                parser.setInput(getInputStream(url), "UTF_8");
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }

            int eventType = XmlPullParser.END_DOCUMENT;
            try {
                eventType = parser.getEventType();
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            }

            SharedPreferences prefs = getSharedPreferences("prefs", Activity.MODE_PRIVATE);
            int i = 0;
            int maxs[] = getResources().getIntArray(R.array.pref_sync_item_amount);
            int max = maxs[prefs.getInt("max", 1)];
            titles = new String[max];
            String text = null;
            boolean item = false;
            while(eventType!= XmlPullParser.END_DOCUMENT && i < max)
            {
                String name = parser.getName();

                switch (eventType)
                {
                    case XmlPullParser.START_TAG:
                        if(name.equals("item"))
                        {
                            item = true;
                        }
                        break;

                    case XmlPullParser.TEXT:
                        text = parser.getText();
                        break;

                    case XmlPullParser.END_TAG:
                        if(name.equals("item"))
                        {
                            item = false;
                            i++;
                        }
                        else if(item) {
                            if (name.equals("title")) {
                                titles[i] = text;
                            } else if (name.equals("description")) {
                                subtitle[i] = text;
                            } else if (name.equals("link")) {
                                links[i] = text;
                            }
                        }

                        break;

                }

                try {
                    eventType = parser.next();
                } catch (XmlPullParserException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }


    }

    public InputStream getInputStream(URL url)
    {
        try {
            return url.openConnection().getInputStream();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}
