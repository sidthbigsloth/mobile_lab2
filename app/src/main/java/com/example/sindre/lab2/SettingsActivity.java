package com.example.sindre.lab2;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

public class SettingsActivity extends AppCompatActivity
{
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        SharedPreferences prefs = getSharedPreferences("prefs", Activity.MODE_PRIVATE);

        EditText url_text = findViewById(R.id.url_edit);
        url_text.setText(prefs.getString("url", ""));

        Spinner maxSpinner = findViewById(R.id.max_edit);
        ArrayAdapter<CharSequence> maxAdapter = ArrayAdapter.createFromResource(this,
                R.array.pref_sync_item_titles, android.R.layout.simple_spinner_item);
        maxAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        maxSpinner.setAdapter(maxAdapter);
        maxSpinner.setSelection(prefs.getInt("max", 1));

        Spinner refreshSpinner = findViewById(R.id.refresh_edit);
        ArrayAdapter<CharSequence> refreshAdapter = ArrayAdapter.createFromResource(this,
                R.array.pref_sync_frequency_titles, android.R.layout.simple_spinner_item);
        refreshAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        refreshSpinner.setAdapter(refreshAdapter);
        refreshSpinner.setSelection(prefs.getInt("refresh", 0));

    }

    public void applySettings(View view)
    {
        SharedPreferences prefs = getSharedPreferences("prefs", Activity.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        if(URLUtil.isValidUrl(((EditText)findViewById(R.id.url_edit)).getText().toString()))
            editor.putString("url", ((EditText)findViewById(R.id.url_edit)).getText().toString());
        editor.putInt("max", ((Spinner)findViewById(R.id.max_edit)).getSelectedItemPosition());
        editor.putInt("refresh", ((Spinner)findViewById(R.id.refresh_edit)).getSelectedItemPosition());
        editor.commit();

        setResult(RESULT_OK, new Intent());

        finish();
    }
}
